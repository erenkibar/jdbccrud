package com.isoft.jdbccrud.dao;

import com.isoft.jdbccrud.database.DBConnection;
import com.isoft.jdbccrud.model.Book;
import com.isoft.jdbccrud.model.ReaderStatusBook;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReaderStatusBookDAO implements DAO<ReaderStatusBook>{
    @Override
    public List<ReaderStatusBook> getAll() throws SQLException {
        Connection connection = DBConnection.getConnection();
        List<ReaderStatusBook> status = new ArrayList<>();
        String GET_ALL = "SELECT reader.username, book.title, reading_status.status " +
                "FROM public.reader_status_book " +
                "JOIN reader ON reader.readerid = reader_id " +
                "JOIN book ON book.bookid = book_id " +
                "JOIN reading_status ON reading_status.statusid = status_id; ";
        PreparedStatement prepStatement = connection.prepareStatement(GET_ALL);
        ResultSet rs = prepStatement.executeQuery();
        while (rs.next()) {
            String username = rs.getString("username");
            String title = rs.getString("title");
            String stat = rs.getString("status");
            status.add(new ReaderStatusBook(username, title, stat));
        }
        return status;
    }

    @Override
    public void save(ReaderStatusBook readerStatusBook) throws SQLException {
        Connection connection = DBConnection.getConnection();
        String INSERT = "INSERT INTO public.reader_status_book( reader_id, book_id, status_id) " +
                "VALUES (" +
                "(SELECT readerid FROM reader WHERE username=?), " +
                "(SELECT bookid FROM book WHERE title=?), " +
                "(SELECT statusid FROM reading_status WHERE status=?)" +
                ");";
        PreparedStatement prepStatement = connection.prepareStatement(INSERT);
        prepStatement.setString(1, readerStatusBook.getUsername());
        prepStatement.setString(2, readerStatusBook.getTitle());
        prepStatement.setString(3, readerStatusBook.getStatus());
        prepStatement.executeUpdate();
    }

    @Override
    public void update(ReaderStatusBook readerStatusBook) throws SQLException {
        Connection connection = DBConnection.getConnection();
        String UPDATE = "UPDATE public.reader_status_book\n" +
                "\tSET status_id=reading_status.statusid\n" +
                "\tFROM reading_status\n" +
                "\tWHERE reading_status.status=? AND reader_status_book_id=?;";
        PreparedStatement prepStatement = connection.prepareStatement(UPDATE);
        prepStatement.setString(1,readerStatusBook.getStatus());
        prepStatement.setLong(2,readerStatusBook.getId());
        prepStatement.executeUpdate();
    }

    @Override
    public void delete(Long id) throws SQLException {
        Connection connection = DBConnection.getConnection();
        String DELETE = "DELETE FROM public.reader_status_book\n" +
                "\tWHERE reader_status_book_id=?;";
        PreparedStatement prepStatement = connection.prepareStatement(DELETE);
        prepStatement.setLong(1,id);
    }
}
