package com.isoft.jdbccrud.dao;

import com.isoft.jdbccrud.database.DBConnection;
import com.isoft.jdbccrud.model.Reader;
import org.postgresql.core.SqlCommand;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ReaderDAO implements DAO<Reader> {

    @Override
    public List<Reader> getAll() throws SQLException {
        List<Reader> readers = new ArrayList<>();
        Connection connection = DBConnection.getConnection();
        String GET_ALL = "SELECT * FROM reader;";
        PreparedStatement prepStatement = connection.prepareStatement(GET_ALL);
        ResultSet rs = prepStatement.executeQuery();
        while (rs.next()) {
            String username = rs.getString("username");
            String email = rs.getString("email");
            String password = rs.getString("password");
            readers.add(new Reader(username, email, password));
        }
        return readers;
    }

    @Override
    public void save(Reader reader) throws SQLException {
        Connection connection = DBConnection.getConnection();
        String INSERT = "INSERT INTO reader (username, email, password) VALUES (?, ?, ?);";
        PreparedStatement prepStatement = connection.prepareStatement(INSERT);
        prepStatement.setString(1, reader.getUsername());
        prepStatement.setString(2, reader.getEmail());
        prepStatement.setString(3, reader.getPassword());
        prepStatement.executeUpdate();
    }

    @Override
    public void update(Reader reader) throws SQLException{
        Connection connection = DBConnection.getConnection();
        String UPDATE = "UPDATE reader SET username=?, email=?, password=? WHERE readerid=?;";
        PreparedStatement prepStatement = connection.prepareStatement(UPDATE);
        prepStatement.setString(1,reader.getUsername());
        prepStatement.setString(2,reader.getEmail());
        prepStatement.setString(3,reader.getPassword());
        prepStatement.setLong(4,reader.getId());
        prepStatement.executeUpdate();
    }

    @Override
    public void delete(Long id) throws SQLException {
        Connection connection = DBConnection.getConnection();
        String DELETE = "DELETE FROM reader WHERE readerid=?;";
        PreparedStatement prepStatement = connection.prepareStatement(DELETE);
        prepStatement.setLong(1, id);
        prepStatement.executeUpdate();
    }
}
