package com.isoft.jdbccrud.dao;

import com.isoft.jdbccrud.database.DBConnection;
import com.isoft.jdbccrud.model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDAO implements DAO<Book> {

    @Override
    public List<Book> getAll() throws SQLException {
        List<Book> books = new ArrayList<>();
        Connection connection = DBConnection.getConnection();
        String GET_ALL = "SELECT book.title, book.isbn, genre.genrename, book.author " +
                "         FROM book " +
                "         JOIN genre " +
                "         ON book.genreid = genre.genreid;";
        PreparedStatement prepStatement = connection.prepareStatement(GET_ALL);
        ResultSet rs = prepStatement.executeQuery();
        while (rs.next()) {
            String title = rs.getString("title");
            String isbn = rs.getString("isbn");
            String genre = rs.getString("genrename");
            String author = rs.getString("author");
            books.add(new Book(title, isbn, genre, author));
        }
        return books;
    }

    @Override
    public void save(Book book) throws SQLException {
        Connection connection = DBConnection.getConnection();
        String INSERT = "INSERT INTO book (title,isbn,genreid,author) VALUES (?, ?, (SELECT genreid FROM genre WHERE genrename=?),?);";
        PreparedStatement prepStatement = connection.prepareStatement(INSERT);
        prepStatement.setString(1, book.getTitle());
        prepStatement.setString(2, book.getIsbn());
        prepStatement.setString(3, book.getGenrename());
        prepStatement.setString(4, book.getAuthor());
        prepStatement.executeUpdate();
    }

    @Override
    public void update(Book book) throws SQLException{
        Connection connection = DBConnection.getConnection();
        String UPDATE = "UPDATE public.book\n" +
                "\tSET title=?, isbn=?, genreid = genre.genreid, author=?\n" +
                "\tFROM genre\n" +
                "\tWHERE genrename=? AND bookid=?;";
        PreparedStatement prepStatement = connection.prepareStatement(UPDATE);
        prepStatement.setString(1,book.getTitle());
        prepStatement.setString(2,book.getIsbn());
        prepStatement.setString(3,book.getAuthor());
        prepStatement.setString(4,book.getGenrename());
        prepStatement.setLong(5,book.getId());
        prepStatement.executeUpdate();
    }

    @Override
    public void delete(Long id) throws SQLException {
        Connection connection = DBConnection.getConnection();
        String DELETE = "DELETE FROM book WHERE bookid=?;";
        PreparedStatement prepStatement = connection.prepareStatement(DELETE);
        prepStatement.setLong(1, id);
        prepStatement.executeUpdate();
    }
}
