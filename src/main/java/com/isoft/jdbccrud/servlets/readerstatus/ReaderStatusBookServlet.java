package com.isoft.jdbccrud.servlets.readerstatus;


import com.isoft.jdbccrud.services.ReaderStatusBookService;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ReaderStatusBookServlet extends HttpServlet {
    private ReaderStatusBookService readerStatusBookService = new ReaderStatusBookService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        readerStatusBookService.insert(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        readerStatusBookService.get(response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        readerStatusBookService.update(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        readerStatusBookService.delete(request, response);
    }
}
