package com.isoft.jdbccrud.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.isoft.jdbccrud.dao.BookDAO;
import com.isoft.jdbccrud.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class BookService {
    private BookDAO bookDAO = new BookDAO();
    private ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger logger = LoggerFactory.getLogger(BookService.class);

    public void get(HttpServletResponse res) throws IOException {
        try{
            List<Book> books = bookDAO.getAll();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            String arrayToJson = objectMapper.writeValueAsString(books);
            PrintWriter out = res.getWriter();
            res.setContentType("application/json");
            out.print(arrayToJson);
            out.flush();
        } catch (SQLException e){
            logger.error(String.valueOf(Level.ERROR),e);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    public void insert(HttpServletRequest req, HttpServletResponse res) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));
        String json = br.lines().collect(Collectors.joining());
        try{
            Book book = objectMapper.readValue(json, Book.class);
            bookDAO.save(book);
        } catch (SQLException e){
            logger.error(String.valueOf(Level.ERROR),e);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));
        String json = br.lines().collect(Collectors.joining());
        try{
            Book book = objectMapper.readValue(json, Book.class);
            bookDAO.update(book);
        } catch (SQLException e){
            logger.error("SQL Exception",e);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String id = req.getRequestURI().substring(req.getRequestURI().lastIndexOf("/") + 1);
        try {
            bookDAO.delete(Long.parseLong(id));
        } catch (SQLException e){
            logger.error(String.valueOf(Level.ERROR),e);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
