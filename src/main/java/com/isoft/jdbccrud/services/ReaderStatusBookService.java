package com.isoft.jdbccrud.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.isoft.jdbccrud.dao.ReaderStatusBookDAO;
import com.isoft.jdbccrud.model.ReaderStatusBook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class ReaderStatusBookService {

    private ReaderStatusBookDAO readerStatusBookDAO = new ReaderStatusBookDAO();
    private ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger logger = LoggerFactory.getLogger(ReaderStatusBookService.class);

    public void get(HttpServletResponse res) throws IOException {
        try {
            List<ReaderStatusBook> statusBooks = readerStatusBookDAO.getAll();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            String arrayToJson = objectMapper.writeValueAsString(statusBooks);
            PrintWriter out = res.getWriter();
            res.setContentType("application/json");
            out.print(arrayToJson);
            out.flush();
        } catch (SQLException e) {
            logger.error(String.valueOf(Level.ERROR), e);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    public void insert(HttpServletRequest req, HttpServletResponse res) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));
        String json = br.lines().collect(Collectors.joining());
        try {
            ReaderStatusBook readerStatusBook = new ReaderStatusBook();
            readerStatusBookDAO.save(readerStatusBook);
        } catch (SQLException e) {
            logger.error(String.valueOf(Level.ERROR), e);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));
        String json = br.lines().collect(Collectors.joining());
        try{
            ReaderStatusBook readerStatusBook = new ReaderStatusBook();
            readerStatusBookDAO.update(readerStatusBook);
        }catch (SQLException e){
            logger.error(String.valueOf(Level.ERROR),e);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }


    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            String id = req.getRequestURI().substring(req.getRequestURI().lastIndexOf("/") + 1);
            readerStatusBookDAO.delete(Long.parseLong(id));
        } catch (SQLException e){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}