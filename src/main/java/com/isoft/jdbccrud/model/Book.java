package com.isoft.jdbccrud.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Book {
    private Long id;
    private String title;
    private String isbn;
    private String genrename;
    private String author;

    public Book(){}

    public Book(Long id, String title, String isbn, String genrename, String author) {
        this.id = id;
        this.title = title;
        this.isbn = isbn;
        this.genrename = genrename;
        this.author = author;
    }

    public Book(String title, String isbn, String genrename, String author) {
        this.title = title;
        this.isbn = isbn;
        this.genrename = genrename;
        this.author = author;
    }

    public Long getId() { return id; }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getGenrename() {
        return genrename;
    }

    public void setGenrename(String genrename) {
        this.genrename = genrename;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", isbn='" + isbn + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
