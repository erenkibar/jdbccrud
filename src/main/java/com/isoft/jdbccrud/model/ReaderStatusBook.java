package com.isoft.jdbccrud.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReaderStatusBook {
    private Long id;
    private String username;
    private String title;
    private String status;

    public ReaderStatusBook(){}

    public ReaderStatusBook(Long id, String username, String readerid, String statusid) {
        this.id = id;
        this.username = username;
        this.title = readerid;
        this.status = statusid;
    }

    public ReaderStatusBook(String username, String readerid, String statusid) {
        this.username = username;
        this.title = readerid;
        this.status = statusid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ReaderStatusBook{" +
                "id=" + id +
                ", bookid=" + username +
                ", readerid=" + title +
                ", statusid=" + status +
                '}';
    }
}
