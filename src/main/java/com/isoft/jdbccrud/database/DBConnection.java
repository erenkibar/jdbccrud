package com.isoft.jdbccrud.database;

import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;


import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
    private static Properties props = new Properties();
    private static BasicDataSource connectionPool = new BasicDataSource();
    private static final Logger logger = LoggerFactory.getLogger(DBConnection.class);

    static {
        try (FileInputStream in = new FileInputStream("/home/eren/IDEAProjects/jdbccrud/src/main/resources/database.properties")){
            props.load(in);
            connectionPool.setDriverClassName(props.getProperty("db.driver.class"));
            connectionPool.setUrl(props.getProperty("db.conn.url"));
            connectionPool.setUsername(props.getProperty("db.username"));
            connectionPool.setPassword(props.getProperty("db.password"));
            connectionPool.setMinIdle(5);
            connectionPool.setMaxIdle(10);
            connectionPool.setMaxOpenPreparedStatements(100);
        } catch(IOException e){
            logger.error(String.valueOf(Level.ERROR),e);
        }
    }

    public static Connection getConnection() throws SQLException {
        return connectionPool.getConnection();
    }

    private DBConnection(){}
}